var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const nodemailer = require('nodemailer');

const config = require('./config.json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

function jsonResponse(response, data) {
    response.setHeader('Content-Type', 'application/json');
    return response.send(JSON.stringify(data));
}

function sendEmailToAddress(email) {
    email = email.trim();

    let transporter = nodemailer.createTransport({
        host: config.host,
        port: config.port,
        secure: config.secure, // true for 465, false for other ports
        auth: {
            user: config.user,
            pass: config.pass
        }
    });

    let mailOptions = {
        from: config.from,
        to: email + ', ' + config.from,
        subject: config.subject,
        text: config.text
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
    });
}

app.post(config.endpoint,
         function(req, res){
             console.log(req.body);
             sendEmailToAddress(req.body[config.emailKey]);
             return jsonResponse(res, {result: 'ok'});
         });

app.listen(process.env.PORT || 3000);
